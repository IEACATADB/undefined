$(window).load(function () {

	var resources = new Array();
	var fire = 0;
	var counter = 0;
	var counter1 = 0;
	var total_workers = 3;
	var free_workers = 3;

	function resource(limit, modifier) {
		this.amount = 0;
		this.income = 0;
		this.limit = limit;
		this.modifier = modifier;
		this.workers = 0;
		this.cost = new Array();
		this.assign_workers = function (amount) {
			if ((amount < 0 && free_workers - amount <= total_workers && this.workers + amount >= 0) || (amount > 0 && free_workers - amount >= 0)) {
				this.workers += amount;
				free_workers -= amount;
				this.income = (this.workers * this.modifier);
			}
		};
		this.update = function () {
			var costcheck = 0;
			var costchecki = 0;
			for (resource in this.cost) {
				
				costcheck++;
				if (resources[resource].amount >= this.cost[resource]) {
					costchecki++;
					
				}
			}
			if (costcheck == costchecki) {
				
				for (var resource in this.cost) {
					resources[resource].amount -= this.cost[resource] * this.income;
				}
				if (this.amount + this.income <= this.limit) {

					this.amount += this.income;
				} else {
					this.amount = this.limit;
				}
			}
		};
		this.click = function () {
			if (this.amount + 1 < this.limit) {
				this.amount++;
			} else {
				this.amount = this.limit;
			}
		};
	}
	/*function invasion(){
	if ( typeof invasion.counter == 'undefined' ) {
	invasion.counter = 0;}
	this.counter++;
	if (this.counter === 1200){

	}
	}*/
	power = new function () {
		this.amount = function () {
			var weapons = 0;
			var manpower = 0;
			var value = 0;
			manpower = total_workers; //for now
			for (var resource in resources) {
				if (resources[resource].isweapon == true) {
					weapons += resources[resource].amount;
				} else if (resources[resource].isunit) {
					manpower += resource.amount;
				}
			}
			value = weapons * manpower; //for now
			return value;
		}
	}

	resources["wood"] = new resource(100, 2);
	resources["stone"] = new resource(100, 1.5);
	resources["tools"] = new resource(50, 0.5);
	resources["tools"].cost = {
		"wood" : 1,
		"stone" : 1
	};
	resources["spears"] = new resource(20, 0.4);
	resources["spears"].cost = {
		"wood" : 1,
		"stone" : 1
	};
	resources["spears"].isweapon = true;


	function UI() {
		function buttons() {
			if (resources["wood"].amount >= 3 && fire < 80) {
				$('#fireb').removeClass('disabled');
			} else {
				$('#fireb').addClass('disabled');
			}
			if (resources["wood"].amount > 0 && resources["stone"].amount > 0) {
				$('#toolsb').removeClass('disabled');
			} else {
				$('#toolsb').addClass('disabled');
			}
			if (resources["tools"].amount > 0.04) {
				$('#treesb').removeClass('disabled');
			} else {
				$('#treesb').addClass('disabled');
			}
			if (resources["tools"].amount > 5 && resources["wood"].amount >= 20) {
				$('#hutsb').removeClass('disabled');
			} else {
				$('#hutsb').addClass('disabled');
			}
		}

		$('#wood').text(resources["wood"].amount + "/" + resources["wood"].limit + " wood ( " + resources["wood"].income + " )");
		$('#stone').text(resources["stone"].amount + " stone ");
		$('#basic_tools').text(Math.round(resources["tools"].amount * 100) / 100 + " basic tools ");
		$('#fire').text(Math.round(fire * 100) / 100 + " fire ");
		$('#workers').text(free_workers + " / " + total_workers + " workers ");
		$('#spears').text(Math.floor(resources["spears"].amount) + " spears ");
		$('#power').text(power.amount() + " estimated battle power ");
		buttons();
	}

	function campfire() {
		if (fire > 0) {
			fire -= 0.033;
			counter++;
			if (counter == 30) {
				if ((total_workers < 6 || resources[4] > 0)) {
					if (Math.random() >= 0) {
						free_workers++;
						total_workers++;
					}
				}
				counter = 0;
			}

		}

	}

	function resource_ticker() {
		counter1++;
		if (counter1 == 10) {

			for (var i in resources) {
				if (resources[i]) {
					resources[i].update();
				}
			}
			counter1 = 0;
		}
	}

	function worker_handler(iclass, n) {
		console.log(iclass);
	}
	$('.workers_add').on('click', function () {
		var iclass = ($(this).parents().attr('class'));
		event.preventDefault();
		resources[iclass].assign_workers(1);
	});
	$('.workers_remove').on('click', function () {
		var iclass = ($(this).parents().attr('class'));
		event.preventDefault();
		resources[iclass].assign_workers(-1);
	});

	$('#stickb').on('click', function () {
		resources["wood"].click();
	});
	$('#stoneb').on('click', function () {
		resources["stone"].click();
	});
	$('#toolsb').on('click', function () {

		resources["tools"].click();
		resources["wood"].amount--;
		resources["stone"].amount--;

	});
	$('#hutsb').on('click', function () {

		resources["tools"].amount--;
		resources["wood"].amount -= 20;

		buildings[0]++;
		resources[4]++;

	});
	$('#fireb').on('click', function () {
		fire += 33;
		wood.amount -= 3;
	});
	window.setInterval(function () {
		resource_ticker();
		UI();
		campfire();

	}, 100);
});
